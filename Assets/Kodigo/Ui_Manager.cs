using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Ui_Manager : MonoBehaviour
{
    public GameObject uI;
    // Start is called before the first frame update
    void Start()
    {
        SetBasicUi(false, "");
    }

    public void SetBasicUi(bool state, string texto)
    {
        uI.GetComponent<TMP_Text>().text = texto;
        uI.SetActive(state);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
