using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movemetn_Behavior : MonoBehaviour
{
    [Header("Movimiento del personaje")]
    public float speedMovement;
    private Vector3 direccion;
    public CharacterController controller;
    public float speedRun;

    [Header("Rotacion de la camara")]
    public Camera playerView;
    //private Vector2 cameraView;
    private float playerRotationX, playerRotationY;
    public float rotationSpeed;

    [Header("Salto")]
    public Vector3 gravity;
    public float jumpHeight;

    public int contador;


    private void Start()
    {

        Cursor.lockState = CursorLockMode.Locked;
    }
    void Update()
    {
        //Gravedad
        gravity.y -= 9.8f * Time.deltaTime;
        controller.Move(gravity * Time.deltaTime);
        if (controller.isGrounded && Input.GetButtonDown("Jump"))
        {
            gravity.y = Mathf.Sqrt(jumpHeight * -2f * -9.8f);
        }
        else if (controller.isGrounded && gravity.y<0)
        {
            gravity.y = -9.8f;
        }/*
        //Movimiento del presonaje
        direccion.x = Input.GetAxisRaw("Horizontal");
        direccion.z = Input.GetAxisRaw("Vertical");
        controller.Move(transform.TransformDirection(direccion) * Time.deltaTime * speedMovement);
        if (Input.GetButton("Run Botton"))
        {
            controller.Move(transform.TransformDirection(direccion) * Time.deltaTime * speedMovement*speedRun);
        }
        //Movimiento de la camara
        cameraView = new Vector2(Input.GetAxisRaw("Mouse X")*cameraSpeed, Input.GetAxisRaw("Mouse Y")*cameraSpeed);
        playerRotationX -= cameraView.y;
        playerRotationY += cameraView.x;
        playerRotationX = Mathf.Clamp(playerRotationX, -40, 40);
        playerView.transform.localRotation = Quaternion.Euler(playerRotationX, 0,0);
        controller.transform.rotation = Quaternion.Euler(0, playerRotationY, 0);

        //Recolleccion
       */

    }

    public void CharacterMove(float vertical, float horizontal)
    {
        //capturar valores del analogo para el movimiento enemigo
        direccion.x = horizontal;
        direccion.z = vertical;
        direccion = transform.TransformDirection(direccion);
        controller.Move(direccion * Time.deltaTime * speedMovement);

    }
    public void RotarCamara (float rotacionCamara)
    {

        //cameraView = new Vector2(0, rotacionCamara);
        playerRotationX -= rotacionCamara;
        playerRotationX = Mathf.Clamp(playerRotationX, -80, 80);
        playerView.transform.localRotation = Quaternion.Euler(playerRotationX, 0, 0);
    }
    public void RotacionPersonaje (float rotacionPersonaje)
    {
        //cameraView = new Vector2(0, rotacionPersonaje);
        //playerRotationY *= rotationSpeed;
        playerRotationY += rotacionPersonaje;
        controller.transform.rotation = Quaternion.Euler(0, playerRotationY*rotationSpeed, 0);
    }
    public void Jump ()
    {
        if (controller.isGrounded && Input.GetButtonDown("Jump"))
        {
            gravity.y = Mathf.Sqrt(jumpHeight * -2f * -9.8f);
        }
    }
}