using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior  : MonoBehaviour
{
    Collider col;
    public Ui_Manager uI;
    public Movemetn_Behavior jugador;
    public Transform cameraPlayer;
    public int distanceRaycast;
    public Transform pickPoint;
    public LayerMask capa1;
    private int valor;
    public Transform playerGun;
    private bool pickGun;
    private bool pickBox;


    
    private void Start()
    {
        pickGun = true;
    }
    private void Update()
    {
       //si recolecta 3 baterias puede abrir la puerta
        if (GetComponent<Player_statistics>().batteryCount>=3)
        {
            if (isCanInteract && col != null)
            {
                uI.SetBasicUi(true, "Pulsa F para interactuar");
                if (Input.GetButtonDown("Accion"))
                {
                    col.GetComponentInParent<controldeanimacion>().OnDoorInteraction();
                }
            }
            else
            {
                uI.SetBasicUi(false,"");
            }
        }
        //Interaccion con RayCast
        Debug.DrawRay(cameraPlayer.position, cameraPlayer.forward*distanceRaycast,Color.green);
        RaycastHit hit;
        if (Physics.Raycast(cameraPlayer.position,cameraPlayer.forward, out hit,distanceRaycast, capa1))
        {
            if (hit.transform.tag=="Box")
            {
                //Indicarle al jugador que puede tomar el objeto
                //uI.SetBasicUi(true, "Presione Click Izquierdo para Interactuar");
                if (Input.GetButtonDown("Accion") &&valor==0)
                {
                    //Toma la caja
                    Debug.Log("esta funcionando?");
                    hit.transform.GetComponent<Rigidbody>().isKinematic = true;
                    hit.transform.parent = pickPoint;
                    hit.transform.position = pickPoint.position;
                    valor = 1;
                    print("coger");
                    pickGun = false;
                    if (playerGun.childCount>0)
                    {
                        playerGun.GetChild(0).gameObject.SetActive(false);
                    }
                }
                else if (Input.GetButtonDown("Accion") &&valor==1)
                {
                    hit.transform.GetComponent<Rigidbody>().isKinematic = false;
                    hit.transform.parent = null;
                    valor = 0;
                    print("soltar");
                    pickGun = true;
                    if (playerGun.childCount > 0)
                    {
                        playerGun.GetChild(0).gameObject.SetActive(true);
                    }

                }
            }
            //Interaccion con el arma
            if (hit.transform.tag=="Gun")
            {
                print("i see a gun");
                if (Input.GetButtonDown("Accion") &&pickGun)
                {
                    hit.transform.GetComponent<Rigidbody>().isKinematic = true;
                    hit.transform.parent = playerGun;
                    hit.transform.localPosition = Vector3.zero;
                    hit.transform.localRotation = Quaternion.Euler(new Vector3(0,-90,0)) ;
                    hit.transform.GetComponent<gunBehavior>().active = true;
                    

                }
            }
        }
        //Tomar el da�o

    }


    public bool isCanInteract = true;
    private void OnTriggerEnter(Collider triggerOfCollision)
    {
        //interaccion con la puerta
        if (triggerOfCollision.tag == "Door")
        {
            uI.SetBasicUi(true, "Necesitas encontrar m�s baterias");
            isCanInteract = true;
            col = triggerOfCollision;
        }
        //interaccion con las baterias
        if (triggerOfCollision.transform.tag == "Battery")
        {
            triggerOfCollision.GetComponent<AudioSource>().PlayOneShot(triggerOfCollision.GetComponent<AudioSource>().clip);
            triggerOfCollision.gameObject.GetComponent<MeshRenderer>().enabled = false;
            triggerOfCollision.transform.GetChild(0).gameObject.SetActive(false);
            triggerOfCollision.gameObject.GetComponent<BoxCollider>().enabled = false;
            GetComponent<Player_statistics>().batteryCount++;
            Debug.Log("Numero de baterias "+GetComponent<Player_statistics>().batteryCount);
            
        }
    }


    private void OnTriggerExit(Collider triggerOfCollision)
    {
        //desactivar interfaz cuando no se necesite
        if (triggerOfCollision.tag == "Door")
        {
            uI.SetBasicUi(false, "");
            isCanInteract = false;
            col = null;
        }

       
    }




}
