using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunBehavior : MonoBehaviour
{
    public GameObject pref_Bullet;
    public bool active;
    public Transform spaw_Bullet;
    public float bulletSpeed;
    public float damage;
    public float gunRange;
    public LayerMask capa1;
    public Canvas retina;
    public Camera cameraPlayer;
    public ParticleSystem muzzeFlash;

    private void Start()
    {
        active = false;

    }
    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && active)
        {
            Shoot();
        }
    }
    void Shoot()
    {
        /*
        GameObject go = Instantiate(pref_Bullet, spaw_Bullet.position, spaw_Bullet.rotation);
        Rigidbody rb = go.GetComponent<Rigidbody>();
        rb.AddForce(go.transform.forward*bulletSpeed,ForceMode.Impulse);
        Destroy(go,2.5f);*/
        muzzeFlash.Play();
        GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
        Debug.DrawRay(transform.position, transform.forward*gunRange, Color.black);
        if (Physics.Raycast(cameraPlayer.transform.position, cameraPlayer.transform.forward, out RaycastHit hit, gunRange,capa1))
        {
            Debug.Log(hit.transform.name);
            if (hit.transform.GetComponent<Interfaz>() !=null)
            {
                hit.transform.GetComponent<Interfaz>().TakeDamage(damage);
            }
        }

    }
}
