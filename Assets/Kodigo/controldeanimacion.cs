using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controldeanimacion : MonoBehaviour
{
    public Animator animationDoor;
    public Animator animationEnemy;
    
    public bool doorState;

    private void Start()
    {
        doorState = true;
        animationDoor.SetBool("state", doorState);
    }

    public void OnDoorInteraction()
    {
        doorState = !doorState;
        animationDoor.SetBool("state", doorState);
    }    
    public void EnemyAnimation(string parametro, float valor)
    {
        animationEnemy.SetFloat(parametro, valor);
    }
}
