using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Movemetn_Behavior))]
[RequireComponent(typeof(Player_statistics))]
public class Player_Controller : MonoBehaviour
{
    private Movemetn_Behavior playerMovemet;
    private Player_statistics statistics;
    public float cameraSpeed;
    private void Start()
    {
        playerMovemet = GetComponent<Movemetn_Behavior>();
        statistics = GetComponent<Player_statistics>();
    }
    private void Update()
    {
        playerMovemet.CharacterMove(Input.GetAxisRaw("Vertical"), Input.GetAxisRaw("Horizontal"));
        playerMovemet.RotarCamara(Input.GetAxisRaw("Mouse Y")*cameraSpeed);
        playerMovemet.RotacionPersonaje(Input.GetAxisRaw("Mouse X")*cameraSpeed);
        playerMovemet.Jump();

    }
}

