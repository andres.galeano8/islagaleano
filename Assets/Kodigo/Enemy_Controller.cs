using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Controller : MonoBehaviour, Interfaz
{
    public Movemetn_Behavior movement;
    public Ui_Manager ui;
    public Transform jugador;
    public controldeanimacion animacion;
    public float rangeOfPursit, rangeOfAtack;
    public LayerMask capa1;
    public Player_statistics statistics;
    private float currentHealt = 100f;
    private void Start()
    {
       movement.GetComponent<Movemetn_Behavior>();
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, jugador.position)<=rangeOfPursit&& Vector3.Distance(transform.position, jugador.position) >= rangeOfAtack)
        {
            ui.SetBasicUi(false,"");
            if (currentHealt>0)
            {
                movement.CharacterMove(1f,0);
                Rotar(1);

            }
            animacion.EnemyAnimation("SpeedMovementAnim", 1f);
            animacion.EnemyAnimation("AttackMovement", 0f);

        }
        else
        {
            animacion.EnemyAnimation("SpeedMovementAnim", 0f);

        }
        if (Vector3.Distance(transform.position, jugador.position) <= rangeOfAtack)
        {
            if (currentHealt>0)
            {

            Rotar(1);
            }
            animacion.EnemyAnimation("AttackMovement", 1f);
        }
    }
    void Rotar (float num)
    {
        Vector3 desireDirection = jugador.position - transform.position;
        if (Vector3.Dot(transform.TransformDirection(Vector3.forward).normalized, desireDirection.normalized)<0.99f)
        {
            if (Vector3.Dot(transform.TransformDirection(Vector3.right).normalized, desireDirection.normalized)>0)
            {
                movement.RotacionPersonaje(num);
            }
            else
            {
                movement.RotacionPersonaje(-num);
            }
        }
    }
    public void Damage ()
    {
        /*Debug.DrawRay(transform.position, transform.forward * rangeOfAtack, Color.blue);
        if (Physics.Raycast(transform.position+new Vector3(0,1,0), transform.forward * rangeOfAtack, out RaycastHit hit, capa1))
        {
            if (hit.transform.tag == "Jugador")
            {
                statistics.HealtController(10, 0);
                Debug.Log(statistics.currentHealt);
                ui.SetBasicUi(true, "Corre");
            }
            Debug.Log(hit.transform.name);
        }*/
        Debug.DrawRay(transform.position, transform.forward * rangeOfAtack, Color.blue);
        if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), transform.forward * rangeOfAtack, out RaycastHit hit, capa1))
        {
            if (hit.transform.GetComponent<Interfaz>() != null)
            {
                hit.transform.GetComponent<Interfaz>().TakeDamage(10f);
                Debug.Log("vida jugador "+statistics.currentHealt);
            }
        }

    }
    public void Die ()
    {
        Destroy(gameObject);
    }
    public void TakeDamage(float damage)
    {
        currentHealt -= damage;
        Debug.Log(currentHealt);
        if (currentHealt<=0)
        {
            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
            animacion.EnemyAnimation("DeathTransition", 1f);
        }
    }
}

