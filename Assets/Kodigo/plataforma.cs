using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataforma : MonoBehaviour
{
    public GameObject platform;
    public GameObject button;
    public Transform minPosition, maxPosition;
    public float platfomrSpeed;
    bool state;
    private void Start()
    {
        state = true;
    }
    void PlatformMovement()
    {
        if (platform.transform.position.y<=maxPosition.transform.position.y&&state)
        {
        platform.transform.Translate(Vector3.up*Time.deltaTime*platfomrSpeed);
            
        }
        else if (platform.transform.position.y >= minPosition.transform.position.y&&!state)
        {
            platform.transform.Translate(Vector3.down * Time.deltaTime * platfomrSpeed);

            
        }
        if (platform.transform.position.y > maxPosition.transform.position.y)
        {
            state = false;
        }
        else if (platform.transform.position.y < minPosition.transform.position.y)
        {
            state = true;
        }

    }
    private void OnTriggerStay(Collider other)
    {
        if (other!= null)
        {
            PlatformMovement();
        }
    }
}
